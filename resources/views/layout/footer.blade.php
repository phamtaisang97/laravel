<div id="footer" class="color-div">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">KHÁCH HÀNG</h4>
						<div>
							<ul>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Trung Tâm Trợ Giúp</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Shopee Mall</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i>Hướng Dẫn Mua Hàng</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Thanh Toán</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Chăm Sóc Khách Hàng</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Chính Sách Bảo Hành</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="widget">
						<h4 class="widget-title">VỀ SHOP</h4>
						<div>
							<ul>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i>Giới Thiệu</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i>Tuyển Dụng</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Điều Khoản </a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Chính Hãng</a></li>
								<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Flash Sales</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
				 <div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">LIÊN HỆ </h4>
						<div>
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p>Thanh Oai - Hà Nội,  Phone: +78 123 456 78</p>
								<p>Uy tín chất lượng hàng đầu Việt Nam.</p>
							</div>
						</div>
					</div>
				  </div>
				</div>
				<div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">THEO DÕI CHÚNG TÔI TRÊN</h4>
						<ul>
							<li>
								<a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>https://www.facebook.com/sang.phamtai</a>
							</li>
							<li>
								<a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i>https://www.youtube.com/</a>
							</li>
							
							
						</ul>
					</div>
				</div>
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- #footer -->
	<div class="copyright">
		<div class="container text-sm-center" style="text-align:center">
			Privacy policy. (&copy;) 2019
		</div> <!-- .container -->
	</div> <!-- .copyright -->
