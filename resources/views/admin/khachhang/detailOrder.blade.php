 @extends('admin.layout.index')
 @section('content')
            <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Đơn đạt hàng
                            <small>Danh sách</small>
                        </h1>
                         @if(session('thongbao'))
                                <div class="alert alert-danger">
                                    {{ session('thongbao') }}
                                </div>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên sản phẩmt</th>
                                <th>Hình ảnh</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; ?>
                        @foreach($orderDetail as $sp)
                           <tr>
                                <td>{{ $i++ }}</td>
                                <td><a href="/thongtinsanpham/{{$sp->product->id}}">{{$sp->product->name}}</a></td>
                                <td><img src="upload/sanpham/{{$sp->product->images}}" alt="" width="70px"></td>
                                <td>{{ number_format($sp->product->price) }} VND</td>
                                <td>x{{$sp->qty}} sản phẩm</td>
                           </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection