 @extends('admin.layout.index')
 @section('content') 
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông tin khách hàng đã order
                            <small>Danh sách</small>
                        </h1>
                    </div>
                      @if(session('thongbao'))
                                <div class="alert alert-danger">
                                    {{ session('thongbao') }}
                                </div>
                        @endif
                    <!-- /.col-lg-12 -->
                    <p>---KH mua hang truc tiep!---</p>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Số điện thoại</th>
                                <th>Tổng tiền</th>
                                <th>Ngay dat hang</th>
                                <th>Xem đơn hàng</th>
                                <th>tuy chon</th>    
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; ?>
                        @foreach($order as $cs)
                            @if( $cs->id_user == null)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$cs->name_customer}}</td>
                                    <td>{{$cs->email}}</td>
                                    <td>{{$cs->address}}</td>
                                    <td>{{$cs->phone}}</td>
                                    <td style="color:red">{{number_format($cs->amount)}}.vnđ</td>
                                    <td>{{$cs->created_at}}</td>
                                    <td><a href="/admin/khachhang/orderDetail/{{$cs->id}}">View</a></td>
                                    <td><a href="/admin/khachhang/delete/{{$cs->id}}">Delete</a></td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    <p>---KH mua hang co tai khoan!---</p>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                            <th>STT</th>
                                <th>Tên</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Số điện thoại</th>
                                <th>Tổng tiền</th>
                                <th>Ngay dat hang</th>
                                <th>Xem đơn hàng</th>
                                <th>tuy chon</th>     
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; ?>
                        @foreach($order as $cs)
                            @if( $cs->id_user !== null)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$cs->customer->name}}</td>
                                    <td>{{$cs->customer->email}}</td>
                                    <td>{{$cs->customer->address}}</td>
                                    <td>{{$cs->customer->phone}}</td>
                                    <td style="color:red">{{number_format($cs->amount)}}.vnđ</td>
                                    <td>{{$cs->created_at}}</td>
                                    <td><a href="/admin/khachhang/orderDetail/{{$cs->id}}">View</a></td>
                                    <td><a href="/admin/khachhang/delete/{{$cs->id}}">Delete</a></td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    {{ $order->links() }}
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection