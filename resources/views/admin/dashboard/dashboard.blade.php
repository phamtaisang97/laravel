@extends('admin.layout.index')    

@section('content') 
	   <!-- Page Content -->
	   <div id="page-wrapper">
		   <div class="container-fluid">
			   <div class="row">
				   <div class="col-sm-4">
					   <a href="#">129</a> san pham
				   </div>
				   <div class="col-sm-4">
					   <a href="#">129</a> don hang
				   </div>
				   <div class="col-sm-4">
					   <a href="#">129</a> user
				   </div>
			   </div>
				<div class="col-sm-3">
					<h3>Khach hang mua nhieu</h3>
					<ul>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<h3>San pham dat hang nhieu</h3>
					<ul>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<h3>San pham comment nhieu</h3>
					<ul>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
						<li>
							<a href="#">
								Pham Sang
							</a>
						</li>
					</ul>
				</div>
		   </div>
		   <!-- /.container-fluid -->
	   </div>
	   <!-- /#page-wrapper -->

@endsection