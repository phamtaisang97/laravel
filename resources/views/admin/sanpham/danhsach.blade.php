 @extends('admin.layout.index')
 @section('content')
            <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sản phẩm
                            <small>Danh sách {{count($sanpham)}}</small>
                        </h1>
                         @if(session('thongbao'))
                                <div class="alert alert-danger">
                                    {{ session('thongbao') }}
                                </div>
                        @endif
                  
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên</th>
                                <th>Hình ảnh</th>
                                <th>Giá</th>
                              
                                <th>Xóa</th>
                                <th>Sửa</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($sanpham as $sp)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $sp->id }}</td>
                                <td>{{ $sp->name }}</td>
                                <td>
                                <img src="upload/sanpham/{{ $sp->images }}" alt="lỗi ảnh" width="70px" height="70px">
					            </td>
                                <td>{{ number_format($sp->price) }}VNĐ</td>
            
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/sanpham/xoa/{{ $sp->id }}" onclick="return confirm('Bạn muốn xóa ?')"> Xóa</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i>  <a href="admin/sanpham/sua/{{ $sp->id }}">Sửa</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>      
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                {{ $sanpham->links() }}
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection