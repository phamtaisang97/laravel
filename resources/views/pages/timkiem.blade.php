@extends('layout.index')
@section('content')
<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space60">&nbsp;</div>
				<div class="row">
					<div class="col-sm-3">
		
						<ul class="aside-menu">
						<p style="text-decoration: underline">LOẠI SẢN PHẨM</p>
							@foreach($loaisp as $l)
							@if(count($l->sanpham)>0)
							<li><a href="loaisanpham/{{ $l->id }}">{{ $l->name }}</a></li>
							@endif
							@endforeach
							
						</ul>
					</div>
						<div class="col-sm-9">
						<p>Từ khóa : %<b>{{$search_txt}}</b>%</p>
						<br>
						<p style="color:red;">	Không tìm thấy sản phẩm !</p> 
						
						</div>
					</div>
				</div> <!-- end section with sidebar and main content -->


			</div> <!-- .main-content -->
		</div> <!-- #content -->
</div> <!-- .container -->
@endsection