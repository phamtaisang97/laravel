@extends('layout.index')
 @section('content') 
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông tin các đơn hàng
                        </h1>
                    </div>
                      @if(session('thongbao'))
                                <div class="alert alert-danger">
                                    {{ session('thongbao') }}
                                </div>
                        @endif
                    <p>---Lịch sử mua hàng ---</p>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                            <th>STT</th>
                                <th>Tên</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Số điện thoại</th>
                                <th>Tổng tiền</th>
                                <th>Ngày đặt</th>
                                <th>Xem đơn hàng</th>
                                <th>Hủy đơn hàng</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; ?>
                        @foreach($order as $cs)
                            @if( $cs->id_user !== null)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$cs->customer->name}}</td>
                                    <td>{{$cs->customer->email}}</td>
                                    <td>{{$cs->customer->address}}</td>
                                    <td>{{$cs->customer->phone}}</td>
                                    <td style="color:red">{{number_format($cs->amount)}}.vnđ</td>
                                    <td>{{$cs->created_at}}</td>
                                    <td><a href="/check-detail/{{$cs->id}}">Xem</a></td>
                                    <td><a href="/del-order/{{$cs->id}}">Hủy đơn hàng</a></td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    {{ $order->links() }}
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection