@extends('layout.index')
@section('content')
<div class="container">
<div id="content">
<div class="row">
<div class="col-sm-9">
	<div class="row">
		<div class="col-sm-5">
			<img src="upload/sanpham/{{ $sanpham->images }}" alt="">
		</div>
		<div class="col-sm-7">
			<div class="single-item-body">
				<p class="single-item-title"><b>{{ $sanpham->name }}</b></p>
				<p class="single-item-price">
					<span style="color: red;">{{ number_format($sanpham->price) }}VNĐ</span>
				</p>
			</div>

			<div class="clearfix"></div>
			<div class="space20">&nbsp;</div>

			<div class="single-item-desc">
			

			</div>
			<div class="space20">&nbsp;</div>

			<div class="single-item-options">
			
				<a class="add-to-cart" href="cart/add_chitiet/{{ $sanpham->id }}"><i class="fa fa-shopping-cart"></i></a>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
<div class="space40">&nbsp;</div>
<div class="woocommerce-tabs">
	<ul class="tabs">
		<li><a href="#tab-description">Mô tả </a></li>
		<li><a href="#tab-reviews">đánh giá</a></li>

	</ul>

	<div class="panel" id="tab-description">
		<p>{!! $sanpham->content !!}</p>
	</div>
	<div class="panel" id="tab-reviews">
		
		@if(!Auth::user())
		<a style="color: red; font-size: 15px;" href="dang-nhap/login">Đăng nhập !</a>
		@else
		<div class="well">
		@if(count($errors)>0)
				<div class="alert alert-danger">
					@foreach($errors->all() as $err)
					{{ $err }} <br>
					@endforeach
				</div>
		@endif
				@if(session('thongbao'))
				<div class="alert alert-danger">
					{{ session('thongbao') }}
				</div>
				@endif
		<div class="form-comment-add">
			<h4>Thêm bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
			<form role="form" action="comment/{{ $sanpham->id }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<textarea id="demo" class="form-control" rows="3" name="content"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Gửi</button>
			</form>   
		</div>
		<div class="form-comment-edit"></div>
		@endif
		@foreach($sanpham->comment as $cm)
		<hr>
		<p style="color: gray;">{{ $cm->user->email }} <span style="color: gray; font-size: 13px">{{ $cm->created_at }}</span>
			@if(isset(Auth::user()->id))
				@if(Auth::user()->id == $cm->user->id)
					<a class="formEdit" href="#" data-id="{{ $cm->id }}" data-content="{{ $cm->content }}"><i style="font-size:20px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
					<a href="/del-comment-fondend/{{ $cm->id }}" onclick="return confirm('Bạn muốn xóa ?')"><i style="font-size:20px;" class="fa fa-trash-o" aria-hidden="true"></i></a>
				@endif
			@endif
		</p>  
		  {!! $cm->content !!} 
		@endforeach
		</div>
	</div>

<div class="beta-products-list">
	<a class="xem-them">Xem thêm<i class="fa fa-caret-down" aria-hidden="true"></i></a>
	<h4 style="
		text-transform: uppercase;
		font-size: 21px;
		font-weight: 600;
		padding-top: 16px;
		display: block;
		border-bottom: 1px solid;
		max-width: 100%;
		margin-bottom:10px;
		">
		Sản phẩm liên quan</h4>

		<div class="row">
			@foreach($sanphamlienquan as $lq)
				<div class="col-sm-4">
					<div class="single-item">
						<div class="single-item-header">
							<a href="thongtinsanpham/{{ $lq->id }}"><img src="upload/sanpham/{{ $lq->images }}" alt="" width="270px" height="320px"></a>
						</div>
						<div class="single-item-body">
							<p class="single-item-title">{{ $lq->name }}</p>
							<p class="single-item-price">
								<span style="color: red;">{{ number_format($lq->price) }}VNĐ</span>
							</p>
						</div>
						<div class="single-item-caption">
							<a class="add-to-cart pull-left" href="thongtinsanpham/{{ $lq->id }}"><i class="fa fa-shopping-cart"></i></a>
							<a class="beta-btn primary" href="thongtinsanpham/{{ $lq->id }}">Xem <i class="fa fa-chevron-right"></i></a>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	<div style="text-align: center;">
        {{ $sanphamlienquan->links() }}
     </div>
</div> <!-- .beta-products-list -->
</div>	
@if(Auth::user())
</div>
@endif


<div class="col-sm-3 aside">
<div class="widget">
	<h3 class="widget-title">Bán chạy nhất</h3>
	<div class="widget-body">
		<div class="beta-sales beta-lists">
			@foreach($sanphamhot as $hot)
			<div class="media beta-sales-item">
				<a class="pull-left" href="thongtinsanpham/{{ $hot->id }}"><img src="upload/sanpham/{{ $hot->images }}" alt=""></a>
				<div class="media-body">
					<p>{{ $hot->name }}</p>
					<p><span style="color:blue">{{$hot->order_count}} đơn hàng</span></p>
					<span class="beta-sales-price">{{ number_format($hot->price) }}.VNĐ</span>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div> <!-- best sellers widget -->
<div class="widget">
	<h3 class="widget-title">Mới nhất</h3>
	<div class="widget-body">
		<div class="beta-sales beta-lists">
			@foreach($sanphammoi as $new)
			<div class="media beta-sales-item">
				<a class="pull-left" href="thongtinsanpham/{{ $new->id }}"><img src="upload/sanpham/{{ $new->images }}" alt=""></a>
				<div class="media-body">
					<p>{{ $new->name }}</p>
					<span class="beta-sales-price">{{ number_format($new->price) }}.VNĐ</span>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div> <!-- best sellers widget -->
</div>
</div>
</div> <!-- #content -->
</div> <!-- .container -->
@endsection
@section('script')
    CKEDITOR.replace('demo', {
        filebrowserImageBrowseUrl : '../../soanthao/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl: '../../soanthao/ckfinder/ckfinder.html?Type="Flash',
        filebrowserImageUploadUrl: '../../soanthao/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../../soanthao/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
    });

	$( ".xem-them" ).click(function( event ) {
		$(".woocommerce-tabs .panel").addClass("show-mota");
		$(".xem-them").hide();
	});

	$( ".formEdit" ).click(function( event ) {
		event.preventDefault();
		$(".form-comment-add").addClass("remove");
		var id = $(this).data('id');
        var content = $(this).data('content');
		$html = `
				<h4>Sửa bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
					<form role="form" action="comment/edit/`+id+`" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_product" value="{{ $sanpham->id }}">
					<div class="form-group">
					<textarea id="demo1" class="form-control" rows="3" name="content">`+content+`</textarea>
					</div>
					<button type="submit" class="btn btn-primary">Lưu</button>
					</form>
				</div>
			`;
		$( ".form-comment-edit" ).html($html);
			CKEDITOR.replace('demo1', {
			filebrowserImageBrowseUrl : '../../soanthao/ckfinder/ckfinder.html?Type=Images',
			filebrowserFlashBrowseUrl: '../../soanthao/ckfinder/ckfinder.html?Type="Flash',
			filebrowserImageUploadUrl: '../../soanthao/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl: '../../soanthao/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
		});
	});

@endsection