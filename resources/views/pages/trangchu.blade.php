@extends('layout.index')
@section('content')
@include('layout.slide')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
				<h4 style="
					text-transform: uppercase;
					font-size: 21px;
					font-weight: 600;
					padding-top: 16px;
					display: block;
					border-bottom: 1px solid;
					max-width: 100%;">
					Sản phẩm bán chạy</h4>
				<div class="beta-products-details">
					<div class="clearfix"></div>
				</div>
				<div class="owl-carousel row owl-theme">
					@foreach($sanphamhot as $sp)
						@if($sp->order_count)
							<div class="col-sm-12 col-xs-12 col-lg-3 item" style="width:100%">
								<div class="single-item">
									<div class="number_count">
										{{$sp->order_count}} đơn đặt
									</div>
									<div class="single-item-header">
										<a href="thongtinsanpham/{{ $sp->id }}"><img src="upload/sanpham/{{ $sp->images }}" alt="lỗi ảnh" width="270px" height="320px"></a>
									</div>
									<div class="single-item-body">
										<p class="single-item-title">{{ $sp->name }}</p>
										<p class="single-item-price">
											<span style="color: red;">{{ number_format($sp->price) }}VNĐ</span>
										</p>
									</div>
									<div class="single-item-caption">
										<a class="add-to-cart pull-left" href="cart/add/{{ $sp->id }}" onclick="alert(' Đã thêm vào 1 giỏ hàng !')" ><i class="fa fa-shopping-cart"></i></a>
										<a class="beta-btn primary" href="thongtinsanpham/{{ $sp->id }}">xem <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						@endif
					@endforeach	
				</div>
	</div> <!-- .beta-products-list -->
		<div class="container">
			
			@foreach($loaisp as $i)
				@if(count($i->sanpham)>0)
					<div class="row">
						<h4 style="
						    margin-bottom: 10px;
							text-transform: uppercase;
							font-size: 21px;
							font-weight: 600;
							padding-top: 16px;
							display: block;
							border-bottom: 1px solid;
							max-width: 100%;">
							<a href="loaisanpham/{{ $i->id }}">
							{{ $i->name }}
							</a>
						</h4>
						@foreach($i->sanpham as $p)
							<div class="col-sm-3 col-xs-12">
								<div class="single-item">
									<div class="single-item-header">
										<a href="thongtinsanpham/{{ $p->id }}"><img src="upload/sanpham/{{ $p->images }}" alt="lỗi" width="270px" height="320px"></a>
									</div>
									<div class="single-item-body">
										<p class="single-item-title">{{ $p->name }}</p>
										<p class="single-item-price">
											<span style="color: red;">{{ number_format($p->price) }}VNĐ</span>
										</p>
									</div>
									<div class="single-item-caption">
										<a class="add-to-cart pull-left" onclick="alert('Đã thêm vào 1 giỏ hàng !')" href="cart/add/{{ $p->id }}"><i class="fa fa-shopping-cart"></i></a>
										<a class="beta-btn primary" href="thongtinsanpham/{{ $p->id }}">Xem <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				@endif
			@endforeach	
		</div>
			<h4 style="
			    margin-bottom: 10px;
				text-transform: uppercase;
				font-size: 21px;
				font-weight: 600;
				padding-top: 16px;
				display: block;
				border-bottom: 1px solid;
				max-width: 100%;">
				Sản phẩm mới</h4>
				<div class="beta-products-details">
					<p class="pull-left"> Tìm thấy {{ count($sanphammoi) }} sản phẩm</p>
					<div class="clearfix"></div>
				</div>
				<div class="row">
					@foreach($sanphammoi as $new)
					<div class="col-sm-3 col-xs-12">
						<div class="single-item">
							<div class="single-item-header">
								<a href="thongtinsanpham/{{ $new->id }}"><img src="upload/sanpham/{{ $new->images }}" alt="lỗi" width="270px" height="320px"></a>
							</div>
							<div class="single-item-body">
								<p class="single-item-title">{{ $new->name }}</p>
								<p class="single-item-price">
									<span style="color: red;">{{ number_format($new->price) }}VNĐ</span>
								</p>
							</div>
							<div class="single-item-caption">
								<a class="add-to-cart pull-left" onclick="alert('Đã thêm vào 1 giỏ hàng !')" href="cart/add/{{ $new->id }}"><i class="fa fa-shopping-cart"></i></a>
								<a class="beta-btn primary" href="thongtinsanpham/{{ $new->id }}">Xem <i class="fa fa-chevron-right"></i></a>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
				<div style="text-align: center;">
                		{{ $sanphammoi->links() }} 
             	</div>
	</div>
</div>
	
@endsection
@section('script')
var owl = $('.owl-carousel');
function myFunction(x) {
  if (x.matches) {
	console.log("Dsd");
	owl.owlCarousel({
		items:1,
		loop:true,
		margin:10,
		autoplay:true,
		autoplayTimeout:1000,
		autoplayHoverPause:true
	});
	$('.play').on('click',function(){
		owl.trigger('play.owl.autoplay',[1000])
	})
	$('.stop').on('click',function(){
		owl.trigger('stop.owl.autoplay')
	})
  } else {
		
		owl.owlCarousel({
			items:4,
			loop:true,
			margin:10,
			autoplay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true
		});
		$('.play').on('click',function(){
			owl.trigger('play.owl.autoplay',[1000])
		})
		$('.stop').on('click',function(){
			owl.trigger('stop.owl.autoplay')
		})
  }
}

var x = window.matchMedia("(max-width: 576px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction)



@endsection