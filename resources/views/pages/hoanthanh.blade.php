<?php 
$data_tien = Cart::getTotal(); $data = Cart::getContent();
?>
<div style="color: green; height:auto; padding-top: 50px; text-align: center;">
	<p> ĐẶT HÀNG THÀNH CÔNG !</p>
	<table class="ht-order" style="width:1000px;margin:auto;">
			<tr style="border:1px solid black">
				<th>Tên sản phẩm</th>
				<th>Hình ảnh</th>
				<th>Số lượng</th>
				<th>Giá</th>
			</tr>
			@foreach($data as $dt)
			<tr>
				<td>{{ $dt->name }}</td>
				<td><img width="100px" src="../../upload/sanpham/{{ $dt->attributes['img'] }}" alt="{{ $dt->attributes['img'] }}" class="pull-left"></td>
				<td><span class="color-gray">X{{ $dt->quantity }}</span></td>
				<td><span class="color-gray your-order-info">Tổng :<span style="color: red;">{{ number_format($dt->price*$dt->quantity) }}VNĐ</span> </span></td>
			</tr>
			@endforeach
	</table>

	<p><span class="color-gray your-order-info">Tổng số tiền bạn phải trả là :<span style="color: red;">{{ number_format($data_tien) }}VNĐ</span> </span> .<br></p>
	<p>Kiểm tra email để biết thêm thông tin đơn hàng ! </p>
	<p>Chúng tôi sẽ giao hàng trong thời gian sớm nhất ! </p><br>
	<p>Cảm ơn bạn đã mua hàng ! </p>
	<p> <br>Phone : 0355161412 <br> email : phamtaisang97@gmail.com</p>
</div>
