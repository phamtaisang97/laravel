<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderDetail extends Model
{
    protected $table = "OrderDetail";
    public $timestamps = true;
    public function product()
    {
        return $this->belongsTo('App\product','id_product','id');
    }
}
