<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    	protected $table = "order";
    	public $timestamps = true;
		
		public function customer()
		{
			return $this->belongsTo('App\User','id_user','id');
		}
		public function detail()
		{
			return $this->hasMany('App\orderDetail','id_order','id');
		}
	
    	
    	
}
