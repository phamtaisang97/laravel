<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\product;
use App\catalog;
use Cart;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function __construct()
	{
		$loaisp = catalog::all();
        $sanphamhot = product::withCount('order')->orderBy('order_count', 'desc')->take(4)->get();
        $sanphammoi = product::orderBy('id','desc')->take(4)->get();
		view()->share('loaisp',$loaisp);
        view()->share('sanphamhot',$sanphamhot);
        view()->share('sanphammoi',$sanphammoi);
        if (Auth::check()) {
            view()->share('nguoidung',Auth::user());
        }
	}
}
