<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\MessageBag;
use App\product;
use App\catalog;
use App\User;
use Hash;
use Cart;
use App\order;
use App\orderDetail;
use DB;
use Illuminate\Support\Facades\Auth;
class PageController extends Controller
{
    public function getIndex()
    {
        $data = Cart::getContent();
        $loaisp = catalog::all();
        $sanphammoi = product::orderByRaw('created_at DESC')->paginate(8);
    	return view('pages.trangchu',['sanphammoi'=>$sanphammoi]);
    }
    public function getGioiThieu()
    {
    	return view('pages.gioithieu');
    }
    public function getLienHe()
    {
    	return view('pages.lienhe');
    }
    public function getChiTietSanPham($id)
    {
        $sanpham = product::find($id);
    	$sanphamlienquan = product::where('id_catalog',$sanpham->id_catalog)->paginate(3);
        $sanphammoi = product::orderByRaw('created_at DESC')->take(4)->get();
    	return view('pages.chitietsanpham',['sanpham'=>$sanpham],['sanphamlienquan'=>$sanphamlienquan],['sanphammoi'=>$sanphammoi]);
    }
    public function getLoaisp($id)
    {
        $sanpham = product::where('id_catalog',$id)->paginate(3);
        $sanphammoi = product::orderByRaw('created_at DESC')->take(3)->get();
    	return view('pages.loaisanpham',['sanpham'=>$sanpham],['sanphammoi'=>$sanphammoi]);
    }
}
