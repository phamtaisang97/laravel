<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\catalog;
use App\product;
class ProductController extends Controller
{
	public function DanhSach(){
		$sanpham = product::orderByRaw('updated_at - created_at DESC')->paginate(8);
		return view('admin.sanpham.danhsach',['sanpham'=>$sanpham]);
	}
	public function GetThem(){
		$theloai = catalog::all();
		return view('admin.sanpham.them',['theloai'=>$theloai]);
	}

	public function PostThem(Request $request){
			$this->validate($request,[
				'name'=>'required',
				'catalog'=>'required',
				'price'=>'required'				
			],[
				'name.required'=>'Bạn chưa nhập tên ',
				'catalog.required'=>'Bạn chưa chọn loại tin ',
				'price.required'=>'Bạn chưa nhập giá cho sản phẩm '
			]);
			$sanpham = new product;
			$sanpham->name = $request->name;
			$sanpham->id_catalog=$request->catalog;
			$sanpham->content = $request->content;
			$sanpham->price = $request->price;
			$sanpham->hot = 1;
			$sanpham->created_at = now();
			if($request->hasFile('images')){
            $file = $request->file('images');
            $duoi = $file->getClientOriginalExtension();
            if($duoi!='jpg' && $duoi !='png' && $duoi !='gif' && $duoi !='jpeg')
            {
                 return redirect('admin/sanpham/them')->with('loi','bạn chỉ được chọn file đuôi jpg,gif,png,jpeg . ');
            }
            
            $name = $file->getClientOriginalName(); //lay ten hinh anh
            
              $Hinh =  str_random(4)."_".$name;
               while (file_exists("upload/sanpham/".$Hinh)) //bat loi chùng anh
            {
               $Hinh =  str_random(4)."_".$name;
            }  

	        //echo $Hinh;
	        $file->move("upload/sanpham",$Hinh);
	        $sanpham->images = $Hinh;
	        $sanpham->save();
	        return redirect('admin/sanpham/them')->with('thongbao','Thêm tin thành công');
   	 	}
	        else
	        {
				$sanpham->Hinh = "";
				return redirect('admin/sanpham/them')->with('thongbao','Bạn chưa thêm hình ảnh');
	        }
        
	}

	public function GetSua($id){
		$theloai = catalog::all();
		$sanpham = product::find($id);
		return view('admin.sanpham.sua',['theloai'=>$theloai],['sanpham'=>$sanpham]);

		
	}
	public function PostSua(Request $request,$id){
		$sanpham = product::find($id);
		$this->validate($request,[
			'name'=>'required',
			'catalog'=>'required',
			'price'=>'required',
		],[
			'name.required'=>'Bạn chưa nhập tên ',
			'catalog.required'=>'Bạn chưa chọn loại tin ',
			'price.required'=>'Bạn chưa nhập giá cho sản phẩm ',
		]);

		$sanpham->name = $request->name;
		$sanpham->id_catalog=$request->catalog;
		$sanpham->content = $request->content;
		$sanpham->price = $request->price;
		$sanpham->created_at = now();
		$sanpham->hot = 1;
		if($request->hasFile('images')){

            $file = $request->file('images');
            $duoi = $file->getClientOriginalExtension();
            if($duoi!='jpg' && $duoi !='png' && $duoi !='gif' && $duoi !='jpeg')
            {
                 return redirect('admin/sanpham/sua')->with('loi','bạn chỉ được chọn file đuôi jpg,gif,png,jpeg . ');
            }
            
            $name = $file->getClientOriginalName(); //lay ten hinh anh
            
              $Hinh =  str_random(4)."_".$name;
            while (file_exists("upload/sanpham/".$Hinh)) //bat loi chùng anh
            {
               $Hinh =  str_random(4)."_".$name;
            }  

	        //echo $Hinh;
	        $file->move("upload/sanpham",$Hinh);
	        $sanpham->images = $Hinh;
	        $sanpham->save();
	        return redirect('admin/sanpham/sua/'.$id)->with('thongbao','Sửa tin thành công ');
			}
			else{
				$sanpham->name = $request->name;
				$sanpham->id_catalog=$request->catalog;
				$sanpham->content = $request->content;
				$sanpham->price = $request->price;
				$sanpham->hot = 1;
				$sanpham->images = $sanpham->images;
				$sanpham->created_at = now();
				$sanpham->save();
				return redirect('admin/sanpham/sua/'.$id)->with('thongbao','Sửa tin thành công ');
			}
	}
	public function GetXoa($id){
		$product = product::find($id);
		foreach($product->order as $pr){
			$pr->delete();
		}
		foreach($product->comment as $pr){
			$pr->delete();
		}	
		$product = product::find($id);
		$product->delete();
		return redirect('admin/sanpham/danhsach')->with('thongbao','Bạn đã xóa thành công');
	}

}