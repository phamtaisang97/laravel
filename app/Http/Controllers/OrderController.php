<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\order;
use App\orderDetail;
use Hash;
use DB;
use Illuminate\Support\Facades\Auth;
class OrderController extends Controller
{
	public function DanhSach(){
		$order = order::paginate(10);
		return view('admin.khachhang.order',['order'=>$order]);
	}
	//ben admin
	public function ChiTietOder($id){
		$orderDetail = orderDetail::where('id_order',$id)->get();
		return view('admin.khachhang.detailOrder',['orderDetail'=>$orderDetail]);
	}

	public function Delete($id){
		$order = order::find($id)->detail;
		foreach($order as $od){
			$od->delete();
		}
		$order = order::find($id);
		$order->delete();
		return redirect('admin/khachhang/order')->with('thongbao','Bạn đã xóa thành công');
	}
	
	//phan home trang chu

	public function checkOrder($id){
		$order = order::where('id_user',$id)->orderByRaw('updated_at - created_at DESC')->paginate(10);
		return view('pages.kiemtradonhang',['order'=>$order]);
	}

	public function ChiTietCheckOder($id){
		$orderDetail = orderDetail::where('id_order',$id)->get();
		return view('pages.chitiet-mot-donhang',['orderDetail'=>$orderDetail]);
	}

	public function Del_home($id){
		$order = order::find($id)->detail;
		foreach($order as $od){
			$od->delete();
		}
		$order = order::find($id);
		$order->delete();
		return redirect()->back()->with('thongbao','Bạn vừa hủy 1 đơn hàng !!');

	}
}