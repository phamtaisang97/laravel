<?php

namespace App\Http\Controllers;
use App\catalog;
use Illuminate\Http\Request;
use App\product;
class SearchController extends Controller
{
    public function postSearch(Request $request){
        $search_txt = $request->search; 
        if($search_txt==null){
            return redirect('/')->with('thongbao','Chưa nhập key tìm kiếm');
        }
        else{
            $sanpham = product::where('name','like',"%$search_txt%")
            ->orWhere('price','like',"%$search_txt%")
            ->paginate(18);
            if(count($sanpham)==0){
                return view('pages.timkiem',['search_txt'=>$search_txt]);
            }
            else{
                return view('pages.loaisanpham',['sanpham'=>$sanpham],['search_txt'=>$search_txt]);
            }
            
        }
        
    }
}
