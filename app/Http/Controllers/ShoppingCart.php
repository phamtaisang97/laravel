<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\MessageBag;
use App\User;
use Hash;
use Cart;
use App\product;
use App\order;
use App\orderDetail;
use Mail;
use App\Mail\ShoppingMail;
use Illuminate\Support\Facades\Auth;
class ShoppingCart extends Controller
{
    //them vao gio hang
    public function getAddCart($id)
    {
        $product = product::find($id);
        Cart::add(array(
        'id' => $id,
        'name' => $product->name,
        'price' => $product->price,
        'quantity' => 1,
        'attributes' => array(
        'img'=>$product->images,
        )
        ));
        return redirect('/cart/show');
    }
    //lay thong tin gio hang
    public function getShowCart()
    {
        $data = Cart::getContent();
        return view('pages.giohang',['data'=>$data]);
    }   
    public function postThanhToan(Request $req)
    {
        // $this->validate($req,
        //     [
        //     'name'=>'required',
        //     'phone'=>'min:10|max:11',
        //     'address'=>'required|min:6|max:255',
        //     ],
        //     [
        //     'name.required'=>'Bạn nhập tên người dùng',
        //     'phone.min'=>'SĐT ít nhất 10 số !',
        //     'phone.max'=>'SĐT nhiều nhất 11 số ! ',
        //     'address.required'=>'Bạn chưa nhập địa chỉ ',
        //     'address.min'=>'địa chỉ tối thiểu 6 ký tự ! ',
        //     'address.max'=>'địa chỉ tối thiểu 255 ký tự ! ',
        //     ]);
      $cart = Cart::getContent();
      $total = Cart::getTotal();
      if(!Auth::user()){
        $order = new order;
        $order->name_customer = $req->name;
        $order->phone = $req->phone;
        $order->address = $req->address;
        $order->email = $req->email;
        $order->level = 0;
        $order->amount = $total;
        $order->created_at = now();
        $order->save();
        foreach($cart as $key => $value) {
            $orderDetail = new orderDetail;
            $orderDetail->id_order = $order->id;
            $orderDetail->id_product = $value->id;
            $orderDetail->qty = $value->quantity;
            $orderDetail->save();
        }

        //send email in laravel
        $mailid = $req->email;
        $subject = 'Đơn Hàng';
        $data = array('email' => $mailid, 'subject' => $subject);
        Mail::send('pages.hoanthanh', $data, function ($message) use ($data) {
        $message->from('phamtaisang97@gmail.com', 'Laravel');
        $message->to($data['email']);
        $message->subject($data['subject']); 
        });

    }
    else{     
        $order = new order;
        $id_us = Auth::user()->id;
        $name = Auth::user()->name;
        $phone = Auth::user()->phone;
        $address = Auth::user()->address;
        $email = Auth::user()->email;
        $order->id_user = $id_us;
        $order->name_customer = $name;
        $order->phone = $phone;
        $order->email = $email;
        $order->address = $address;
        $order->level = 1;
        $order->created_at = now();
        $order->amount = $total;
        $order->save();
        foreach($cart as  $value) {
            $orderDetail = new orderDetail;
            $orderDetail->id_order = $order->id;
            $orderDetail->id_product = $value->id;
            $orderDetail->qty = $value->quantity;
            $orderDetail->save();
        }
        //send email in laravel
        $mailid = $email;
        $subject = 'Đơn Hàng';
        $data = array('email' => $mailid, 'subject' => $subject);
        Mail::send('pages.hoanthanh', $data, function ($message) use ($data) {
        $message->from('phamtaisang97@gmail.com', 'Laravel');
        $message->to($data['email']);
        $message->subject($data['subject']); 
        });
        }
        
        return redirect('cart/complete');

    }
    public function getComplete()
    {
        return view('pages.hoanthanh');
    }
    //xoa gio hang
    public function xoaCart($id)
    {
        Cart::remove($id);
           return redirect('cart/show');
    }
}
