<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\MessageBag;
use App\User;
use Hash;
use DB;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function getDangNhap()
    {
        return view('pages.dangnhap');
    }
    public function postDangNhap(Request $request)
    {
      $rules = [
            'email' =>'required|email',
            'password' => 'required|min:6'
        ];
        $messages = [
            'email.required' => 'Email là trường bắt buộc',
            'email.email' => 'Email không đúng định dạng',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $email = $request->input('email');
            $password = $request->input('password');

            if( Auth::attempt(['email' => $email, 'password' =>$password])) {
                $user = Auth::User();
                 if($user->level == 1){
                    return redirect()->intended('admin/theloai/danhsach');
                }
                else
                return redirect()->intended('/');
            } else {
                $errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
                return redirect()->back()->withInput()->withErrors($errors);
            }
        }
    }
    public function getDangKy()
    {
        return view('pages.dangky');
    }
    public function postDangKy(Request $request)
    {
       $this->validate($request,
            [
            'name'=>'required',
            'phone'=>'min:10|max:11',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:6|max:32',
            'passwordAgain'=>'required|same:password'
            ],
            [
            'name.required'=>'Bạn nhập tên người dùng',
            'phone.min'=>'SĐT ít nhất 10 số !',
            'phone.max'=>'SĐT nhiều nhất 11 số ! ',
            'email.required' =>'Bạn chưa nhập Email ',
            'email.email'=>'Bạn chưa nhập đúng định dạng email ',
            'email.unique'=>'Email đã tồn tại ',
            'password.required'=>'Bạn chưa nhập mật khẩu ',
            'password.min'=>'Mật khẩu phải có ít nhất 6 ký tự',
            'password.max'=>'Mật khẩu chỉ được 32 ký tự nhé ',
            'passwordAgain.required'=>'Chưa nhập lại mật khẩu ',
            'passwordAgain.same'=>'Mật khẩu nhập lại không khớp nhé !'

            ]);
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->password = Hash::make($request->password);
            $user->phone = $request->phone;
            $user->level = 0;
            $user->save();
            return redirect()->back()->with('thongbao',' Chúc mừng ban đã đăng ký thành công ^^ ');
    }
    public function getprofile()
    {
        return view('pages.thongtinnguoidung');
    }
    public function postprofile($id,Request $request)
    {
        $this->validate($request,
        [
        'name'=>'required',
        ],
        [
        'name.required'=>'Bạn nhập tên người dùng',
        ]);
        $user =User::find($id);
        $user->name= $request->name;
        $user->address= $request->address;
        $user->phone = $request->phone;
        $user->level= 0;
    if($request->checkbox=="on")
    {
        $this->validate($request,
        [
        'password'=>'required|min:6|max:32',
        'passwordAgain'=>'required|same:password'
        ],
        [
        'password.required'=>'Bạn chưa nhập mật khẩu ',
        'password.min'=>'Mật khẩu phải có ít nhất 6 ký tự',
        'password.max'=>'Mật khẩu chỉ được 32 ký tự nhé ',
        'passwordAgain.required'=>'Chưa nhập lại mật khẩu ',
        'passwordAgain.same'=>'Mật khẩu nhập lại không khớp nhé !'
        ]);
         $user->password= bcrypt($request->password);
        }
        $user->save();
            return redirect()->back()->with('thongbao',' Đã thay đổi thông tin ! ');
    }
    public function getdangxuat()
    {
        Auth::logout();
         return redirect('/');
    }
}
