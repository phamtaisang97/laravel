<?php
//tao database
Route::get('create-table-category',function(){
	Schema::create('category',function($table){
		$table->increments('id');
		$table->string('name',200);
		$table->timestamps();
	});
	echo "created category !!";
});
Route::get('create-table-product',function(){
	Schema::create('product',function($table){
		$table->increments('id');
		$table->integer('id_category')->unsigned();
        $table->foreign('id_category')->references('id')->on('category');
		$table->string('name',100);
		$table->longtext('content',255);
		$table->string('images');
		$table->float('price');
		$table->integer('hot')->default(1);;
		$table->integer('view')->default(0);
		$table->timestamps();
	});
	echo "created product";
});
Route::get('create-table-user',function(){
	Schema::create('users', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('phone');
			$table->string('address');
			$table->integer('level');
            $table->rememberToken();
            $table->timestamps();
        });
	echo "created user";
});
Route::get('create-table-order',function(){
	Schema::create('order',function($table){
		$table->increments('id');
		$table->integer('id_user')->unsigned();
		$table->foreign('id_user')->references('id')->on('users');
		$table->integer('qty');
		$table->float('amount');
		$table->integer('phone');
		$table->text('address');
		$table->text('level');
		$table->text('note');
		$table->timestamps();
	});
	echo "created order";
});
Route::get('create-table-orderDetail',function(){
	Schema::create('OrderDetail',function($table){
        $table->increments('id');
		$table->integer('id_order')->unsigned();
		$table->foreign('id_order')->references('id')->on('order');
		$table->integer('id_product')->unsigned();
        $table->foreign('id_product')->references('id')->on('product');
		$table->timestamps();
	});
	echo "created OderDetail ";
});
Route::get('create-table-comment',function(){
	Schema::create('comment',function($table){
		$table->integer('id_user')->unsigned();
        $table->foreign('id_user')->references('id')->on('users');
        $table->increments('id');
        $table->integer('id_product')->unsigned();
        $table->foreign('id_product')->references('id')->on('product');
		$table->integer('content');
		$table->date('date');
		$table->timestamps();
	});
	echo "created comment";
});

//chinh sua bang 
Route::get('create-table',function(){
	Schema::create('test',function($table){
		$table->increments('id');
		$table->string('name',200);
		$table->timestamps();
	});
	echo "vừa tạo bảng test";
});
Route::get('rename-table',function(){
	Schema::rename('test','ahih');
	echo "doi ten thanh cong !!";
});
Route::get('del-table',function(){
	Schema::drop('ahih');
	echo "da xoa bang";
});
Route::get('add-column',function(){
	Schema::table('users',function($table){
		$table->string('order_id');
	});
	echo "add thanh cong !!";
});
Route::get('edit-test',function(){
	Schema::table('test',function($table){
		$table->droColumn('hot');
	});
	echo "edit thanh cong !!";
});
//insert dữ liệu mau
Route::get('insert-data',function(){
	DB::table('catalog')->insert([
		['name' => 'Dien thoai'],
		['name' => 'May tinh'],
		['name' => 'tivi'],
	]);
	echo "da instert !";
});
Route::get('test',function(){
	$data = App\oderDetail::find(1)->product;
	dd($data);
});
// router page admin
Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){
	Route::group(['prefix'=>'theloai'],function(){
		Route::get('danhsach','CategoryController@DanhSach');
		Route::get('them','CategoryController@GetThem');
		Route::post('them','CategoryController@PostThem');
		Route::get('sua/{id}','CategoryController@GetSua');
		Route::post('sua/{id}','CategoryController@PostSua');
		Route::get('xoa/{id}','CategoryController@GetXoa');
		Route::post('search','CategoryController@search');
	}); /*het the loai*/

	Route::group(['prefix'=>'sanpham'],function(){
		Route::get('danhsach','ProductController@DanhSach');
		Route::get('them','ProductController@GetThem');
		Route::post('them','ProductController@PostThem');
		Route::get('sua/{id}','ProductController@GetSua');
		Route::post('sua/{id}','ProductController@PostSua');
		Route::get('xoa/{id}','ProductController@GetXoa');
	});

	Route::group(['prefix'=>'khachhang'],function(){
		Route::get('order','OrderController@DanhSach');
		Route::get('delete/{id}','OrderController@Delete');
		Route::get('orderDetail/{id}','OrderController@ChiTietOder');				
	});

	Route::group(['prefix'=>'comment'],function(){
		Route::get('danhsach','CommentController@DanhSach');
		Route::get('noidung/{id}','CommentController@NoiDung');
		Route::get('xoa/{id}','CommentController@GetXoa');
	});
	Route::group(['prefix'=>'user'],function(){
		Route::get('danhsach','UserController@DanhSach');
		Route::get('xoa/{id}','UserController@GetXoa');
		Route::get('them','UserController@GetThem');
		Route::post('them','UserController@PostThem');
	});

	
	Route::group(['prefix'=>'dashboard'],function(){
		Route::get('home','DashBoardController@index');
	});

	

});

// Router page Home
		Route::get('/','PageController@getIndex');
		Route::get('gioithieu','PageController@getGioiThieu');
		Route::get('lienhe','PageController@getLienHe');
		Route::get('thongtinsanpham/{id}','PageController@getChiTietSanPham');
		Route::get('loaisanpham/{id}','PageController@getLoaisp');
		Route::post('comment/{id}','CommentController@postComment');
		
		Route::group(['prefix'=>'cart'],function(){
			Route::get('add/{id}','ShoppingCart@getAddCart');
			Route::get('add_chitiet/{id}','ShoppingCart@getAddCart');
			Route::get('show','ShoppingCart@getShowCart');
			Route::post('thanhtoan','ShoppingCart@postThanhToan');
			Route::get('xoa/{id}','ShoppingCart@xoaCart');
			Route::get('update','ShoppingCart@getUpdateCart');
			Route::get('complete','ShoppingCart@getComplete');
		});

		Route::group(['prefix'=>'dang-nhap'],function(){
			Route::get('login','LoginController@getDangNhap');
			Route::post('login','LoginController@postDangNhap');
			Route::get('profile/{id}','LoginController@getprofile');
			Route::post('profile/{id}','LoginController@postprofile');
			Route::get('dangxuat','LoginController@getdangxuat');
			Route::get('dangky','LoginController@getDangKy');
			Route::post('dangky','LoginController@postDangKy');
		});
		Route::get('email',function(){
			return view('pages.email');
		});

		//home
		Route::post('/search','SearchController@postSearch');
		Route::get('/search','SearchController@getSearch');
		Route::get('check/{id}','OrderController@checkOrder');
		Route::get('check-detail/{id}','OrderController@ChiTietCheckOder');	
		Route::get('del-order/{id}','OrderController@Del_home');			
		Route::get('del-comment-fondend/{id}','CommentController@GetXoaFontend');
		Route::post('comment/edit/{id}','CommentController@EditFrontend');
		